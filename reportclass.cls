%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  Filename: reportclass.cls
%%%  Class File for Endgraf Report
%%%  --- 
%%%  Written by Canggih Puspo Wibowo
%%%  [canggihpw@gmail.com]
%%%  ---
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%======================================
%% Identification
%======================================
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{reportclass}[2020/02/07 Endgraf Report Latex Template]

%======================================
%% Require packages
%======================================
\RequirePackage{graphicx} %use graphics
\RequirePackage{xcolor} %color
\RequirePackage{fancyhdr} % header and footer
\RequirePackage{multicol} %merge column
\RequirePackage{multirow} %merge rows
\RequirePackage{pgfplots}%drawing
\RequirePackage{pgfplotstable} %draw table using pgf
\RequirePackage{pgf-pie}%pie chart
\RequirePackage{wheelchart}%pie chart
\RequirePackage{colortbl} %tabel color
\RequirePackage{anyfontsize} %use any size of font
\RequirePackage{amsmath}
\RequirePackage{tabularx}


%======================================
%% Base class
%======================================
\LoadClass[12pt]{report}


%======================================
%% Additional packages
%======================================
\usepackage{fontspec} %use xetex
\usepackage[a4paper,papersize={8.27in ,11.69in}]{geometry} % page layout 21 x 29.7
\usepackage[tablename=TABLE,figurename=FIGURE]{caption} % setting for caption
\usepackage{subcaption} % setting caption in subfigure
\usepackage{booktabs} % to create good looking table
\usepackage{enumitem} %config enumerate & itemize
\usepackage{tcolorbox} %coloring box
\usepackage{titlesec} %change format section
\usepackage{tikz}%drawing
\usepackage{lipsum}
\usepackage{ragged2e} % justify text
\usepackage{etoolbox} % for xappto command
\usepackage{longtable} % table span pages
\usepackage{tikz-network}
\usepackage{xstring}% parsing
%\usepackage{expl3}
%\usepackage[pages=some]{background}
\usetikzlibrary{calc}
% Use each plot as separate process latex
%\usepgfplotslibrary{external} 
%\tikzexternalize


%======================================
%% Additional setup
%======================================
\geometry{
	top=3cm,
	left=2cm,
	right=2cm,
	bottom=3cm,
	footskip=2.5cm,
	headsep=1cm
}

\DeclareTextCommandDefault{\textunderscore}{%
	\leavevmode \kern.06em\vbox{\hrule width .3em}}

% Set font
\setmainfont{Tinos}
\setsansfont{Lato}
%\newfontfamily\titlefont{Gentium Book Basic}

\setlist{nolistsep} %no space before itemize/enumerate
% set section
%\titleformat{\part}{\huge\sffamily \bfseries}{\partname \ \thepart\\}{0.5em}{}
%\titleformat{\chapter}{\flushleft\huge\sffamily \bfseries}{\chaptername \ \thechapter:}{0.5em}{}
%\titlespacing*{\chapter}{0pt}{-50pt}{30pt}
\titleformat{\section}{\flushleft\huge\sffamily \bfseries}{\thesection.}{0.5em}{}
\titleformat{\subsection}{\flushleft\large\sffamily \bfseries}{\thesubsection.}{0.5em}{}
%\titlespacing\subsection{0pt}{1em}{2pt}

% Renew text
\renewcommand{\partname}{Bagian}
\renewcommand{\chaptername}{Bab}
\renewcommand{\figurename}{Gambar}
\renewcommand{\tablename}{Tabel}
\renewcommand{\contentsname}{Daftar Isi}
\renewcommand\thepart{\Alph{part}}

% new column type
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

\pgfplotsset{compat=1.14}

% Arraystretch
\renewcommand{\arraystretch}{1.5}%

% define kedata color
\definecolor{kedatacolor}{RGB}{0,135,159}
\definecolor{blueish}{rgb}{0.565,0.886,1}  % blue-ish
\definecolor{greenish}{rgb}{0.565,1,0.886}  % green-ish
\definecolor{darkgray}{rgb}{0.15,0.15,0.15}  % very dark gray
\definecolor{lightgray}{rgb}{0.6,0.6,0.6}  % light gray

% Tikz style
\usetikzlibrary{shapes}
\tikzstyle kedatashape=[line width=5pt,color=kedatacolor,opacity=0.035,scale=7]


%======================================
%% INPUT INFORMATION
%======================================
\newcommand{\headreport}[2]{
	\newcommand{\@keyword}{#1}
	\newcommand{\@daterange}{#2}
}

%======================================
%% Page Style Header and Footer
%======================================
\fancypagestyle{coverstyle}{%
	\fancyhf{}
	\fancyfoot[C]{
		\sffamily
		\begin{tcolorbox}[fontupper=\color{white},colback=kedatacolor,colframe=kedatacolor,arc=0pt,outer arc=0pt]
			\fontsize{6}{6}\selectfont 
			\begin{minipage}{0.39\textwidth}
				\textbf{PT. Kedata Indonesia Digital} \\
				Gg. Anggur No. 14b, Krodan, Maguwoharjo, Depok, \\
				Sleman, Daerah Istimewa Yogyakarta 55281
			\end{minipage}
			\begin{minipage}{0.2\textwidth}
				\textbf{Telepon}\\+62 821 3446 0090
			\end{minipage}
			\begin{minipage}{0.15\textwidth}
				\textbf{Website}\\kedata.online
			\end{minipage}
			\begin{minipage}{0.15\textwidth}
				\centering
				\includegraphics[width=2cm]{kedata-white.png}
			\end{minipage}
		\end{tcolorbox}
	}
	
	\renewcommand{\headrulewidth}{0pt}
	\renewcommand{\footrulewidth}{0pt}
}
\fancypagestyle{fancystyle}{%
	\fancyhf{}
	\fancyfoot[C]{
		\sffamily
		\begin{tcolorbox}[fontupper=\color{white},colback=kedatacolor,colframe=kedatacolor,arc=0pt,outer arc=0pt]
			\fontsize{6}{6}\selectfont 
			\begin{minipage}{0.39\textwidth}
				\textbf{PT. Kedata Indonesia Digital} \\
				Gg. Anggur No. 14b, Krodan, Maguwoharjo, Depok, \\
				Sleman, Daerah Istimewa Yogyakarta 55281
			\end{minipage}
			\begin{minipage}{0.2\textwidth}
				\textbf{Telepon}\\+62 821 3446 0090
			\end{minipage}
			\begin{minipage}{0.15\textwidth}
				\textbf{Website}\\kedata.online
			\end{minipage}
			\begin{minipage}{0.15\textwidth}
				\centering
				\includegraphics[width=2cm]{kedata-white.png}
			\end{minipage}
			\begin{minipage}{0.1\textwidth}
				\flushright \bfseries \normalsize \thepage
			\end{minipage}
		\end{tcolorbox}
	}
	\fancyhead[L]{
		\sffamily
		Keyword\\ \@keyword
	}
	\fancyhead[R]{
		\sffamily
		Date range\\ \@daterange
	}
	
	\renewcommand{\headrulewidth}{0pt}
	\renewcommand{\footrulewidth}{0pt}
}
%\thispagestyle{empty}
\pagestyle{fancystyle}

%======================================
%% Create flushleft and roman font for paragraph
%======================================
\newcommand{\partext}[1]{
	\justify \rmfamily \normalsize #1	
}

%======================================
%% tikzpicture no indent
%======================================
\let\svtikzpicture\tikzpicture
\def\tikzpicture{\noindent\svtikzpicture}


%======================================
%% COVER
%======================================
\newcommand{\createcover}[3]{
	\thispagestyle{coverstyle}
	\centering
	\sffamily
	\vspace*{15em}
	\includegraphics[width=0.2\textwidth]{kedata-logo.png} \vspace{2em}\\
	{\fontsize{40}{40}\selectfont \bfseries ENDGRAF REPORT} \vspace{2em}\\
	{\textbf{KEYWORD} \\ #1 \ | \ #2} \vspace{1em}\\
	{\textbf{DATE RANGE} \\ #3} \\
	\begin{tikzpicture}[remember picture,overlay,auto]% height (0.65-0.5)
		\node[kedatashape,draw,circle] at (-0.5\textwidth,0.65\textheight) {};
		\node[kedatashape,draw,rectangle,rotate=75] at (-0.2\textwidth,0.5\textheight) {};
		\node[kedatashape,draw,diamond] at (-0.1\textwidth,0.7\textheight) {};
		\node[kedatashape,draw,regular polygon,regular polygon sides=5] at (0.1\textwidth,0.55\textheight) {};
		\node[kedatashape,draw,regular polygon,rotate=15,regular polygon sides=6] at (0.5\textwidth,0.6\textheight) {};

		\node[kedatashape,draw,circle] at (-0.4\textwidth,0.2\textheight) {};
		\node[kedatashape,draw,rectangle,rotate=75] at (-0.2\textwidth, 0.3\textheight) {};
		\node[kedatashape,draw,diamond] at (0.1\textwidth,0.25\textheight) {};
		\node[kedatashape,draw,regular polygon,regular polygon sides=5] at (0.4\textwidth,0.1\textheight) {};
		
		\node[kedatashape,draw,regular polygon,rotate=15,regular polygon sides=6] at (-0.45\textwidth,0.4\textheight) {};
		\node[kedatashape,draw,diamond] at (0.45\textwidth,0.4\textheight) {};

		\node[kedatashape,draw,circle] at (-0.45\textwidth,-0.2\textheight) {};
		\node[kedatashape,draw,rectangle,rotate=75] at (-0.25\textwidth, -0.3\textheight) {};
		\node[kedatashape,draw,diamond] at (0.15\textwidth,-0.25\textheight) {};
		\node[kedatashape,draw,circle] at (0.45\textwidth,-0.1\textheight) {};

		\node[kedatashape,draw,rectangle,rotate=75] at (-0.5\textwidth,-0.015\textheight) {};
		\node[kedatashape,draw,diamond] at (-0.1\textwidth,-0.05\textheight) {};

		\node[kedatashape,draw,regular polygon,regular polygon sides=5] at (0.6\textwidth,-0.4\textheight) {};
	\end{tikzpicture}
	\clearpage
}